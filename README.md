# IDP-Z3 Websocket wrapper

A websocket wrapper for the IDP-Z3 reasoning system.
Goal: enable IDP-Z3 as a SaaS by having a universal interface.

Contains the `idp_engine` module directly for now, until the IDP-Z3Py API has been merged. (https://gitlab.com/krr/IDP-Z3/-/merge_requests/137)

An example of a message sent through the API:

```
{"method": "modelexpand", "models": [{"Bordering": [["Belgium", "France"], ["Belgium", "Germany"], ["Belgium", "Netherlands"], ["France", "Germany"]], "ColorOf": {"Belgium": "Red", "France": "Green", "Germany": "Blue", "Netherlands": "Green"}, "NogIets": {"('{(Belgium', 'Belgium) ')": "Red", "('Belgium', 'France) ')": "Red", "('Belgium', 'Germany) ')": "Red", "('Belgium', 'Netherlands) ')": "Red", "('France', 'Belgium) ')": "Red", "('France', 'France) ')": "Red", "('France', 'Germany) ')": "Red", "('France', 'Netherlands) ')": "Red", "('Germany', 'Belgium) ')": "Red", "('Germany', 'France) ')": "Red", "('Germany', 'Germany) ')": "Red", "('Germany', 'Netherlands) ')": "Red", "('Netherlands', 'Belgium) ')": "Red", "('Netherlands', 'France) ')": "Red", "('Netherlands', 'Germany) ')": "Red", "('Netherlands', 'Netherlands) ')": "Red"}, "q": false, "p": false}], "success": true, "id": null}
```

## Explanation

This uses an old version of an IDP-Z3 API I wrote at one point. In the future, this should be replaced by KeBAB.
Many aspects of the websocket were, unfortunately, also hastely written to reach a deadline. Not sure if it's worth it to redo everything from scratch however.
Currently, this API is in use in only a few projects:
* [IDP-Z3 by example](https://interactive-idp.gitlab.io/)
* [My catan generator](https://vadevesi.gitlab.io/catan-generator.gitlab.io/)
* [The IDP-Z3 demos](https://vadevesi.gitlab.io/idp-z3-demos.gitlab.io/)

Sometime in the future, [SLI](https://gitlab.com/EAVISE/sli/SLI) will be able to compile into wasm allowing us to bake an IDP application in the client directly.

Sorry to whoever needs to maintain this in the future. :-(
