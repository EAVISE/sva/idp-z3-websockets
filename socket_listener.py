import asyncio
import websockets
from idp_engine.API import KB
import os
import signal
import json


def key_to_str(dictionary):
    if len(dictionary) == 0:
        return dictionary
    new_dict = {}
    if (isinstance(list(dictionary.items())[0], str) or
       isinstance(list(dictionary.items())[0], tuple)):
        if isinstance(list(dictionary.values())[0], list):
            new_dict = {str(key).strip('}{ '): val for key, val in dictionary.items()}
        else:
            new_dict = {str(key).strip('}{ '): val.strip('}{ ') for key, val in dictionary.items()}
    else:
        new_dict = dictionary
    return new_dict


def clean_models(models):
    """
    We need to clean up the models to be sent via json.
    E.g., all dict keys should be converted into string, as json does not
    support using tuples as keys.
    """
    new_models = []
    for model in models:
        for symbol in model:
            if isinstance(model[symbol], dict):
                new_dict = key_to_str(model[symbol])
                model[symbol] = new_dict
        new_models.append(model)
    return new_models


def clean_dicts(dicts):
    """
    We also need to clean up the pos and negdicts, as their internal
    dictionaries may not have tuples as keys.
    """
    new_dicts = []
    for dictionary in dicts:
        new_dict = {}
        for symbol in dictionary.keys():
            if symbol in ['goal_symbol', ' relevant']:
                continue
            if isinstance(dictionary[symbol], dict):
                new_dict[symbol] = key_to_str(dictionary[symbol])
            elif isinstance(dictionary[symbol], bool):
                new_dict[symbol] = dictionary[symbol]
            elif isinstance(dictionary[symbol], list):
                new_dict[symbol] = dictionary[symbol]
        new_dicts.append(new_dict)
    return new_dicts


async def create_KB(theory):
    kb = KB()
    print(theory)
    kb.from_str(theory)
    return kb


async def handler(websocket):
    KB = None
    async for message in websocket:
        try:
            # message = await websocket.recv()
            # Receive message, and set its ID to null if it has none.
            message = json.loads(message)
            if 'id' not in message:
                message['id'] = None

            # Check which method is requested and execute.
            if message['method'] == 'create':
                try:
                    KB = await create_KB(message['theory'])
                    success = True
                    print("KB initialised")
                except Exception as e:
                    success = False
                    print("KB not initialised")
                msg = {'method': 'create', 'success': success,
                       'id': message['id']}
                await websocket.send(json.dumps(msg))

            elif message['method'] == 'modelexpand':
                try:
                    models = KB.model_expand(message['number'])
                    models = clean_models(models)
                    print(models)
                    msg = {'method': 'modelexpand',
                           'models': models,
                           'success': True if len(models) > 0 else False,
                           'id': message['id']}
                    await websocket.send(json.dumps(msg))
                except AttributeError as e:
                    # KB was incorrectly initialised
                    raise e
                    pass

            elif message['method'] == 'propagate':
                out = KB.propagate()
                out = clean_dicts(out)
                msg = {'method': 'propagate',
                       'posdict': out[0],
                       'negdict': out[1],
                       'success': success,
                       'id': message['id']}
                await websocket.send(json.dumps(msg))

            elif message['method'] == 'explain':
                out = KB.custom_main('pretty_print(Theory(T,S).explain())')
                msg = {'method': 'explain',
                       'succes': True,
                       'explanation': out.replace('Structure formula', ''),
                       'id': message['id']}
                await websocket.send(json.dumps(msg))

            elif message['method'] == 'setval':
                symbol = message['symbol']
                val = message['val']
                KB.symbols[symbol].value = val

            elif message['method'] == 'setenumeration':
                symbol = message['symbol']
                enum = message['enum']
                KB.symbols[symbol].enumeration = enum

        except Exception as e:
            msg = {'method': 'failure',
                   'success': False,
                   'id': message['id']}
            await websocket.send(json.dumps(msg))
            # Never crash the handler
            print('Socket-wide error:', e)


async def main():
    if 'PORT' in os.environ:
        # Serve on a specific port (e.g., for Heroku)
        PORT = os.environ['PORT']
    else:
        PORT = 8765
    loop = asyncio.get_running_loop()
    stop = loop.create_future()
    loop.add_signal_handler(signal.SIGTERM, stop.set_result, None)

    print(f'Starting listener on port {PORT}')
    async with websockets.serve(handler, "", PORT):
        await stop  # run until SIGTERM

if __name__ == "__main__":
    asyncio.run(main())
