
import asyncio
import websockets
import json
theory = """
vocabulary {
type Country := {Belgium, France, Germany, Netherlands}
type Color := {Red, Blue, Green}
Bordering : (Country * Country) -> Bool
ColorOf : (Country) -> Color
NogIets: (Country * Country) -> Color
q,p: -> Bool

}

structure {
Bordering := {(Belgium, France), (Belgium, Netherlands), (Belgium, Germany),
(France, Germany), (Netherlands, Germany)}.

}

theory {
!c1, c2 in Country: Bordering(c1, c2) => ColorOf(c1) ~= ColorOf(c2).
ColorOf(Belgium) = Red.
ColorOf(Germany) = Blue.
ColorOf(France) = Green.

!c1 in Country: NogIets(c1, c1) = Red.

// p() > q().
}
"""
theory="""
vocabulary V {
  type gate := {A, B, C, D, E}
  type wire := {K, L, N, M, O, P, Q, R}
  AND: (gate) -> Bool
  OR: (gate) -> Bool
  XOR: (gate) -> Bool

  FirstInput: (gate) -> wire
  SecondInput: (gate) -> wire
  Output: (gate) -> wire

  On: (wire) -> Bool

  StartOn: (wire) -> Bool
  StartOff: (wire) -> Bool
}
structure S:V {
  FirstInput := {A -> K, B -> N, C -> L, D -> M, E -> Q}.
  SecondInput := {A -> L, B -> M, C -> K, D -> N, E -> O}.
  Output := {A -> N, B -> P, C -> O, D -> Q, E -> R}.
  AND := {C, D}.
  XOR := {A, B}.
  OR := {E}.
}
theory T:V {

!x in wire: StartOn(x) => On(x).
!g in gate: AND(g) => (On(FirstInput(g)) & On(SecondInput(g)) <=> On(Output(g))).



}
procedure main() {
    pretty_print(model_expand(T, S))
}
"""

async def hello():
    uri = "ws://localhost:8765"
    # uri = "wss://idpsocket.herokuapp.com/"
    # uri = "ws://reasoning-websocket.cs.kuleuven.be:8765"
    # uri = "wss://reasoning-websocket.cs.kuleuven.be/wss"
    # uri = "wss://websocket.simonvandevelde.be"
    async with websockets.connect(uri) as websocket:
        await websocket.send(json.dumps({'method': 'create',
                                         'theory': theory}))
        result = await websocket.recv()
        # await websocket.send(json.dumps({'method': 'modelexpand',
        #                                  'number': 1}))
        # print(await websocket.recv())
        # await websocket.send(json.dumps({'method': 'propagate',
        #                                  'number': 1}))
        # print(await websocket.recv())

        # name = input("What's your name? ")

        # await websocket.send(name)
        # print(f">>> {name}")

        # greeting = await websocket.recv()
        # print(f"<<< {greeting}")

if __name__ == "__main__":
    asyncio.run(hello())
